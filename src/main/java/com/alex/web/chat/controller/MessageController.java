package com.alex.web.chat.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/rest-api")
public class MessageController {

    @PostMapping
    @Secured("ROLE_ADMIN")
    public List<String> messages() {
        return List.of("messageOne", "messageTwo");
    }

    @GetMapping
    @Secured("ROLE_USER")
    public String message() {
        return "messageOne";
    }
}
