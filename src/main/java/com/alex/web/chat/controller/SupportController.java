package com.alex.web.chat.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inner-api")
public class SupportController {

    @GetMapping
    public String ok() {
        return "{\"health\": \"ok\"}";
    }

}
